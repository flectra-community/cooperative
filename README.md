# Flectra Community / cooperative

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[cooperator](cooperator/) | 2.0.1.6.0| Manage your cooperators
[cooperator_portal](cooperator_portal/) | 2.0.1.0.1| Show cooperator information in the website portal
[cooperator_website](cooperator_website/) | 2.0.1.1.2|     This module adds the cooperator subscription form    allowing to subscribe for shares online.    


